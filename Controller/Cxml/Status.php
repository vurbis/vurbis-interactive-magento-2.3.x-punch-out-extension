<?php

namespace Vurbis\Punchout\Controller\Cxml;

use Vurbis\Punchout\Controller\BaseController;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;
use Vurbis\Punchout\Model\Configuration;
use Magento\Framework\Module\FullModuleList;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\State;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Integration\Model\Oauth\Token;


/**
 * Status Controller
 */
class Status extends BaseController
{
    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var FullModuleList
     */
    protected $fullModuleList;

    /**
     * @var ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var State
     */
    protected $appState;

    /**
     * @param Context $context
     * @param Configuration $configuration
     * @param FullModuleList $fullModuleList
     * @param State $appState
     */
    public function __construct(
        Context $context,
        Configuration $configuration,
        FullModuleList $fullModuleList,
        ProductMetadataInterface $productMetadata,
        State $appState,
        JsonFactory $resultJsonFactory,
        Token $tokenModel
    ) {
        parent::__construct($context, $resultJsonFactory, $tokenModel);
        $this->configuration = $configuration;
        $this->fullModuleList = $fullModuleList;
        $this->productMetadata = $productMetadata;
        $this->appState = $appState;
    }


    /**
     * Request action
     */
    public function execute()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isGet()) {
                return $this->createJsonResponse([
                    'success' => false,
                    'message' => 'Invalid request method. This action only accepts GET requests.'
                ], 405); 
            }

            $authToken = $request->getHeader('Authorization');
            if (empty($authToken) || !$this->isValidToken($authToken)) {
                return $this->createJsonResponse([
                    'success' => false,
                    'message' => 'Invalid access token.'
                ], 401); 
            }

    
            $installedModules = $this->getAllModulesWithVersions();

            $isInstalled = isset($installedModules['Vurbis_Punchout']);
            $version = $isInstalled ? $installedModules['Vurbis_Punchout'] : null;

            $system_status = [
                'plugin' => [
                    'module' => 'Vurbis_Punchout',
                    'is_installed' => $isInstalled,
                    'version' => $version
                ],
                'pluginConfig' => [
                    'is_active' => $this->safeGet('isPluginEnabled'),
                    'api_url' =>  $this->safeGet('getApiUrl'),
                    'supplier_id' => $this->safeGet('getSupplierId'),
                    'is_cron_enabled' => $this->safeGet('isCronEnabled'),
                    'cron_delete_customers_older_days' => $this->safeGet('getCronDays')
                ],
                'magento' => [
                    'edition' => $this->productMetadata->getEdition() ?? 'Unknown',
                    'version' => $this->productMetadata->getVersion() ?? 'Unknown',
                    'operating_environment_mode' => $this->appState->getMode() ?? 'Unknown'
                ],
                'environment' => [
                    'php_version' => PHP_VERSION ?? 'Unknown',
                    'operating_system' => php_uname() ?? 'Unknown',
                    'software' => $_SERVER['SERVER_SOFTWARE'] ?? 'Unknown'
                ],
                'modulesList' => $installedModules
            ];

            $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
            return $result->setHeader('Content-Type', 'application/json')->setContents(json_encode($system_status));

        } catch (LocalizedException $e) {
            return $this->createJsonResponse([
                'success' => false,
                'message' => 'Failed to load status: ' . $e->getMessage()
            ], 500);
        }

    }


    /**
     * Get list of all installed non-default Magento modules and their versions
     *
     * @return array
     */
    private function getAllModulesWithVersions()
    {
        $allModules = [];
        foreach ($this->fullModuleList->getAll() as $moduleName => $moduleInfo) {
            // Include only modules that have a setup_version (non-null)
            if ($moduleInfo['setup_version'] !== null) {
                $allModules[$moduleName] = $moduleInfo['setup_version'];
            }
        }
        return $allModules;
    }

    /**
     * Get configuration values, return 'Unknown' if not set. 
     * Avoids endpoint breaking when one value is not found.
     *
     * @param string $methodName
     * @return mixed
     */
    private function safeGet($methodName)
    {
        try {
            return $this->configuration->{$methodName}();
        } catch (LocalizedException $e) {
            return 'Unknown';
        }
    }

}