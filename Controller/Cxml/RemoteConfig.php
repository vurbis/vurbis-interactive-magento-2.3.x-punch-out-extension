<?php

namespace Vurbis\Punchout\Controller\Cxml;

use Vurbis\Punchout\Controller\BaseController;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Vurbis\Punchout\Model\Configuration;
use Psr\Log\LoggerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Integration\Model\Oauth\Token;



/**
 * RemoteConfig Controller
 */
class RemoteConfig extends BaseController
{
    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @param Context $context
     * @param Configuration $configuration
     * @param LoggerInterface $logger
     * @param Json $jsonSerializer
     */
    public function __construct(
        Context $context,
        Configuration $configuration,
        LoggerInterface $logger,
        Json $jsonSerializer,
        JsonFactory $resultJsonFactory,
        Token $tokenModel
    ) {
        parent::__construct($context, $resultJsonFactory, $tokenModel);
        $this->configuration = $configuration;
        $this->logger = $logger;
        $this->jsonSerializer = $jsonSerializer;
    }


    /**
     * Request action
     */
    public function execute()
    {
        try {
            $request = $this->getRequest();

            if (!$request->isPost()) {
                return $this->createJsonResponse([
                    'success' => false,
                    'message' => 'Invalid request method. This action only accepts POST requests.'
                ], 405); 
            }

            $authToken = $request->getHeader('Authorization');
            if (empty($authToken) || !$this->isValidToken($authToken)) {
                return $this->createJsonResponse([
                    'success' => false,
                    'message' => 'Invalid access token.'
                ], 401); 
            }

            // Get content and check is array
            $content = $request->getContent();
            $configData = !empty($content) ? $this->jsonSerializer->unserialize($content) : null;
            if (!is_array($configData)) {
                return $this->createJsonResponse([
                    'success' => false,
                    'message' => 'The provided configuration data is invalid. Please send a valid JSON payload with the configuration settings.'
                ], 400); 
            }

            $this->configuration->setConfig($configData);

            return $this->createJsonResponse([
                'success' => true,
                'message' => 'Configuration updated successfully.'
            ], 200); 

        } catch (LocalizedException $e) {
            $this->logger->error('Vurbis Punchout - Failed to update punchout configuration: ' . $e->getMessage());
            return $this->createJsonResponse([
                'success' => false,
                'message' => 'Failed to update punchout configuration: ' . $e->getMessage()
            ], 500);
        }

    }

}