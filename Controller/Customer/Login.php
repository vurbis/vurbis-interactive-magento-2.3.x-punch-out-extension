<?php

namespace Vurbis\Punchout\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\UrlFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Vurbis\Punchout\Model\Configuration;
use Vurbis\Punchout\Model\Punchout;


/**
 * Login controller
 */
class Login extends Action
{
    /**
     * Customer session
     * @var Session
     */
    protected $session;

    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var Punchout
     */
    protected $punchout;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var PhpCookieManager
     */
    protected $cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    protected $cookieMetadataFactory;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var CustomerRepository
     */
    protected $customerRepository;
    
    /** 
     * @param Context $context
     * @param Session $session
     * @param UrlFactory $urlFactory
     * @param Configuration $configuration
     * @param Punchout $punchout
     * @param CustomerRepository $customerRepository
     * @param CustomerFactory $customerFactory
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Session $session,
        UrlFactory $urlFactory,
        Configuration $configuration,
        Punchout $punchout,
        CustomerRepository $customerRepository,
        CustomerFactory $customerFactory,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->session = $session;
        $this->url = $urlFactory->create();
        $this->configuration = $configuration; 
        $this->punchout = $punchout;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Login customer
     */
    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            // log out customer
            if ($this->session->isLoggedIn()) {
                $lastCustomerId = $this->session->getId();
                $this->session->logout()->setLastCustomerId($lastCustomerId);
            }
            $this->session->setPunchoutIsOci(false);
            $req = $this->getRequest();
            $punchoutSession = $req->getParam('session');
            $username = $req->getParam('username');
            $password = $req->getParam('password');
            if (!isset($password)) {
                $password = $req->getParam('pass');
            };
            $apiUrl = $this->configuration->getApiUrl();
            $authenticated = false;
            if (!$punchoutSession) {
                $supplier_id = $this->configuration->getSupplierId();
                $url = $apiUrl . "/punchout/" . $supplier_id . "/login";
                $res = $this->punchout->post($url, [
                    'query' => $req->getParams(),
                    'body' => $req->getContent(),
                    'servers' => $req->getServer() // includes headers -> HTTP_ properties
                ]);
                if (!isset($res->id)) {
                    throw new LocalizedException(
                        __('Username and password could not be found in Vurbis marketplace.')
                    );
                }
                $punchoutSession = $res->id;
                $username = $res->username;
                $password = $res->password;
                $authenticated = true;
                $is_clean_customer_id = false;
                if(isset($res->config)) {
                    if(isset($res->config->clean_customer_id)) {
                        $is_clean_customer_id = $res->config->clean_customer_id;
                    }
                }
                $this->session->setPunchoutCleanCustomerId($is_clean_customer_id);
                $this->session->setPunchoutIsOci(true);
            }

            if (!$punchoutSession) {
                throw new LocalizedException(__('Punchout session is required.'));
            }

            if (!$username) {
                throw new LocalizedException(__('Username is required.'));
            }
            if (!$password) {
                throw new LocalizedException(__('Password is required.'));
            }

            if (!$authenticated) {
                $authRes = $this->punchout->post(
                    $apiUrl . "/punchout/authenticate",
                    [
                        'username' => $username,
                        'password' => $password,
                        'session' => $punchoutSession,
                    ]
                );
                if (!$authRes->authenticated) {
                    throw new LocalizedException(__('Authentication failed.'));
                }
            }

            try {
                $customer = $this->customerRepository->get(
                    $username,
                    $this->storeManager->getWebsite()->getWebsiteId()
                );
            } catch (NoSuchEntityException $e) {
                throw new LocalizedException(__('Failed to login.'));
            }

            $this->_eventManager->dispatch('customer_data_object_login', ['customer' => $customer]);

            $this->session->setCustomerDataAsLoggedIn($customer);
            $this->session->regenerateId();

            if ($this->cookieManager->getCookie('mage-cache-sessid')) {
                $metadata = $this->cookieMetadataFactory->createCookieMetadata();
                $metadata->setPath('/');
                $this->cookieManager->deleteCookie('mage-cache-sessid', $metadata);
            }

            // Add punchout session ID to customer session
            $this->session->setPunchoutSession($punchoutSession);

            // Fake request method to trigger version update for private content
            $this->_request->setMethod(\Laminas\Http\Request::METHOD_POST);
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $defaultUrl = $this->url->getUrl('/', ['_secure' => true]);
        $resultRedirect->setUrl($defaultUrl);
        return $resultRedirect;
    }
}
