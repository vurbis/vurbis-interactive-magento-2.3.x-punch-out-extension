<?php
namespace Vurbis\Punchout\Plugin;

/**
 * CsrfValidatorSkip Plugin
 */
class CsrfValidatorSkip
{
    /**
     * Check the module controller.
     *
     * @param \Magento\Framework\App\Request\CsrfValidator $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\ActionInterface $action
     */
    public function aroundValidate(
        $subject,
        \Closure $proceed,
        $request,
        $action
    ) {
        if ($request->getControllerModule() != 'Vurbis_Punchout') {
            return $proceed($request, $action);
        }
    }
}
