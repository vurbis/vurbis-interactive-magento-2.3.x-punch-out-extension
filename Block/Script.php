<?php

namespace Vurbis\Punchout\Block;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Element\Template;

class Script extends Template
{
    /**
     * Return script URL
     *
     * @return string
     */
    public function getScriptUrl()
    {
        return $this->_urlBuilder->getUrl('punchout/cxml/script');
    }

    /**
     * Avoid output if disabled
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->isPunchoutEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * Punchout enabled
     *
     * @return bool
     */
    protected function isPunchoutEnabled()
    {
        return !empty($this->_scopeConfig->getValue(
            'vurbis_punchout/api/punchout_enabled',
            ScopeInterface::SCOPE_STORE
        ));
    }
}
