<?php

namespace Vurbis\Punchout\Api;

/**
 * ApiUpdateResponseInterface Api
 */
interface ApiUpdateResponseInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
     * Constants defined for keys of the data array. Identical to the name of the getter in snake case
     */
    public const RESULT = 'result';

    public const FIELD = 'field';

    public const ERROR = 'error';
    /**#@-*/

    /**
     * Get Id.
     *
     * @return string|null
     */
    public function getId();

    /**
     * Set Id.
     *
     * @param string $id
     * @return $this
     */
    public function setId($id = null);

    /**
     * Get Field.
     *
     * @return string|null
     */
    public function getProps();

    /**
     * Set Field.
     *
     * @param string $field
     * @return $this
     */
    public function setField($field = null);
}
